import React from "react";
import Menu from './components/Menu';
import Home from "./views/Home";
import ViewArticle from "./views/ViewArticle";
import { BrowserRouter,Switch,Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import Article from "./views/Article";
import Category from "./views/Category";
import Author from "./views/Author";
import ShowPage from "./views/ShowPage";
export default function App() {
  return(
    <BrowserRouter>
    <Menu/>
    <Switch>
      <Route path="/" exact component={Home}></Route>
      <Route path="/category" exact component={Category}></Route>
      <Route path="/author" exact component={Author}></Route>
      <Route path='/article/:id' component={ViewArticle} />
      <Route path='/article' component={Article} />
      <Route path='/articles' component={ShowPage} />
      <Route path='/update/article/:id' component={Article} />
    </Switch>
    </BrowserRouter>
  )
}
