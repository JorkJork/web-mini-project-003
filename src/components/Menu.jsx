import React from "react";
import {Navbar,Form,Button,Nav,FormControl, Container} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
function Menu() {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand className="mx-5" href="#home">AMS Redux</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={NavLink} to="/"><b>Home</b></Nav.Link>
          <Nav.Link as={NavLink} to="/article"><b>Article</b></Nav.Link>
          <Nav.Link as={NavLink} to="/author"><b>Author</b></Nav.Link>
          <Nav.Link as={NavLink} to="/category"><b>Category</b></Nav.Link>
             <select class="form-select" aria-label="Default select example">
              <option value="English">English</option>
              <option value="Khmer">Khmer</option>
            </select>

        </Nav>
        <Form className="d-flex mx-3">
         <FormControl
        type="search"
        placeholder="Search"
        className="mr-2"
        aria-label="Search"
      />
      <Button variant="outline-success" className="mx-2">Search</Button>
    </Form>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default Menu;
