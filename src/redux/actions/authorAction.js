import { fetch_all_author } from "../../services/author.service"


const FETCH_ALL_AUTHOR ="FETCH_ALL_AUTHOR"
export const fetchAllAuthor =()=>{
    return async dispatch=>{
        const result = await fetch_all_author()
        dispatch({
            type:FETCH_ALL_AUTHOR,
             payload: result
        })
    }
}