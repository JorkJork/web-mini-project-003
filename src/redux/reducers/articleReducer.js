

const  initState ={
    articles:[]
}
export default function articleReducer(state=initState,{type,payload}){
    switch(type){
        case "FETCH_ALL_ARTICLES":
            return{
                ...state,
                articles : payload
            }
        default:
            return state;    
    }
}