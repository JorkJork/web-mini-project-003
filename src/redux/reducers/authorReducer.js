const  initState ={
    author:[]
}
export default function authorReducer(state=initState,{type,payload}){
    switch(type){
        case "FETCH_ALL_AUTHOR":
            return{
                ...state,
                author : payload
            }
        default:
            return state;    
    }
}