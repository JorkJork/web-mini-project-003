import { applyMiddleware, combineReducers, createStore } from "redux";
import articleReducer from "../reducers/articleReducer";
import categoryReducer from "../reducers/categoryReducer";
import authorReducer from "../reducers/authorReducer";
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const rootReducers = combineReducers({
    articleReducer,
    categoryReducer,
    authorReducer
})

export const store = createStore(rootReducers, applyMiddleware(thunk, logger))