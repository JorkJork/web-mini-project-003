import { API } from "./API"

export const fetch_all_category = async() => {
    try {
        const result = await API.get("/category")
        return result.data.data
    } catch (error) {
        console.log("fetch_all_category error:", error);
    }
}
export const fetchCategoryById = async (id) => {
    let response = await API.get('/category/' + id)
    return response.data.data
}

export const updateCategoryById = async (id, newCategory) => {
    let response = await API.put('/category/' + id, newCategory)
    return response.data.message

}
export const deleteCategory = async (id) => {
    let response = await API.delete('/category/' + id)
    return response.data.message
}

export const postCategory = async (category) => {
    let response = await API.post('/category', category)
    return response.data.message
}

