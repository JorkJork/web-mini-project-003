
import {Container,Form,Row,Col,Button,Table} from 'react-bootstrap'
import React,{useState,useEffect} from "react";
import {fetchAuthorById,updateAuthorById,deleteAuthor, postAuthor } from "../services/author.service";
import { uploadImage } from '../services/author.service';
import { fetchAllAuthor } from '../redux/actions/authorAction';
import { useDispatch, useSelector } from 'react-redux'

export default function Author() {
  
    const [name,setName] =useState('')
    const [email,setEmail] =useState("")
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    const[idUpdate,setIdUpdate]=useState(0);
    const [btnAddSave,setBtnAddSave] =useState('Add')
    let addSave;
    btnAddSave==='Add'?addSave=true:addSave=false
    
    const dispatch = useDispatch()
    const {author} = useSelector(state=>state.authorReducer)
    console.log("author :",author);
    useEffect(()=>{
        dispatch(fetchAllAuthor())
    },[])
     
      

     const onDelete = (id)=>{
        deleteAuthor(id).then((message)=>{
          let newAuthor = author.filter((author)=>author._id !== id)
          alert(message)
          author = newAuthor
        }).catch(e=>console.log(e))
      }
    
    const clearName = React.useRef()
    const clearEmail = React.useRef()
    const defaultImg ="https://designshack.net/wp-content/uploads/placeholder-image.png"
    
    const onAdd= async(e)=>{
       e.preventDefault()
       let author ={
         name,email
       }
       if(imageFile){
         let url= await uploadImage(imageFile)
         author.image=url;
         setImageURL(defaultImg);
       }
       postAuthor(author).then(message=>alert(message))
       clearName.current.value="";
       clearEmail.current.value="";
       setName('')
       setEmail('')
    }

    const showText=(id)=>{
      setIdUpdate(id);
      setBtnAddSave('Save')
      fetchAuthorById(id).then(author=>{
        setName(author.name)
        setEmail(author.email)
        setImageURL(author.image)
      })
    }
  

  const onUpdate = async(id)=>{
    let author = {
       name,email
    }
    if(imageFile){
       let url = await uploadImage(imageFile)
       author.image = url
    }
    updateAuthorById(id,author).then(message=>alert(message))
    setBtnAddSave('Add')
    clearName.current.value="";
    clearEmail.current.value="";
    setImageURL(defaultImg);
       setName('')
       setEmail('')
   }
    
    
  
    return (
        <div>
       <Container>
       <Row> 
       <Col className="mt-5">
       <h1>Author</h1>
        <Form >
            <Form.Group controlId="category">
            <Form.Label><b>Author Name</b></Form.Label>
            <Form.Control ref={clearName} type="text" placeholder="Author Name" value={name}
              onChange={(e)=>setName(e.target.value)}
            />
            <Form.Label  className="mt-2"><b>Email</b></Form.Label>
            <Form.Control  ref={clearEmail} name="email" type="email" placeholder="email"  value={email} 
                onChange={(e)=>setEmail(e.target.value)}
            />
            </Form.Group>
           
            <Button size="sm"variant="primary" className="mt-2"
               onClick={addSave?onAdd:()=>onUpdate(idUpdate)}
            >{btnAddSave}</Button>

        </Form>
        </Col>
        
         <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
        </Row> 
        <Table striped bordered hover className="my-4">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Image</th>
            <th style={{width:140}}>Action</th>
          </tr>
        </thead>
        <tbody>
        {
        author.map((item,index)=>(
        <tr key={index}>
            <th scope="row">{item._id}</th>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td >{<img style={{height:150,width:150}} src={item.image}/>}</td>
            <td>
              <Button size="sm mr-2 mb-2 mt-2" variant="warning"
                onClick={
                  ()=>{
                   showText(item._id)
                  }
                }
              >Edit</Button>{" "}
              <Button size="sm " variant="danger" 
              // disabled={btnDisable===1}
              onClick={()=>onDelete(item._id)} > Delete</Button>
            </td>
        </tr>
        ))}
           
        </tbody>
      </Table>   
      </Container> 
        </div>
    )
}
