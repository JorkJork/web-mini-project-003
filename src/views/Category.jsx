import React, { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllCategory} from '../redux/actions/categoryAction'
import {fetchCategoryById,updateCategoryById,deleteCategory, postCategory } from "../services/category.service";
import { Button,Table, Form, Card ,Container,Row,Col} from "react-bootstrap";



export default function Category() {


  const [name,setName] =useState('')
  const [createdAt,setCreatedAt] =useState("")
  const [updatedAt,setUpdatedAt] =useState('')
  const[idUpdate,setIdUpdate]=useState(0);
  const [btnAddSave,setBtnAddSave] =useState('Add')
  let addSave;
  btnAddSave==='Add'?addSave=true:addSave=false
  const clearName = React.useRef()
  const clearCreatedAt = React.useRef()
  const clearUpdatedAt = React.useRef()
  

    const dispatch = useDispatch()
    const {category} = useSelector(state=>state.categoryReducer)
    console.log("Category :",category);
    useEffect(()=>{
        dispatch(fetchAllCategory())
    },[])
    

    const onAdd= async(e)=>{
      e.preventDefault()
      let category ={
        name,createdAt,updatedAt
      }
      
      postCategory(category).then(message=>alert(message))
      clearName.current.value="";
      clearCreatedAt.current.value="";
      clearUpdatedAt.current.value="";
      setName('')
      setCreatedAt('')
      setUpdatedAt('')
   }

     
    const onDelete = (id)=>{
      deleteCategory(id).then((message)=>{
        let newCategory = category.filter((category)=>category._id !== id)
        alert(message)
        category = newCategory
      }).catch(e=>console.log(e))
    }
  
 
    
    const showText=(id)=>{
      setIdUpdate(id);
      setBtnAddSave('Save')
      fetchCategoryById(id).then(category=>{
        setName(category.name)
        setCreatedAt(category.createdAt)
        setUpdatedAt(category.updatedAt)
      })
    }
  

    const onUpdate = async(id)=>{
      let category = {
         name,createdAt,updatedAt
      }
     
      updateCategoryById(id,category).then(message=>alert(message))
      setBtnAddSave('Add')
      clearName.current.value="";
      clearCreatedAt.current.value="";
      clearUpdatedAt.current.value="";
     
         setName('')
         setCreatedAt('')
         setUpdatedAt('')
     }
      

    return (

        <Container>
        <h1>Category</h1>
        <Form>
            <Form.Group controlId="name">
              <Form.Label><b>Name</b></Form.Label>
              <Form.Control 
                ref={clearName}
                type="text" 
                value={name}
                onChange={(e)=>setName(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="createdAt">
              <Form.Label><b>created_date</b></Form.Label>
              <Form.Control 
              ref={clearCreatedAt}
                type="text" 
                value={createdAt}
                onChange={(e)=>setCreatedAt(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="createdAt">
              <Form.Label><b>updated_date</b></Form.Label>
              <Form.Control 
              ref={clearUpdatedAt}
                type="text" 
                value={updatedAt}
                onChange={(e)=>setUpdatedAt(e.target.value)}
                />
              <Form.Text className="text-muted">
              </Form.Text>
            </Form.Group>

            <Button size="sm"variant="primary" className="mt-2"
               onClick={addSave?onAdd:()=>onUpdate(idUpdate)}
            >{btnAddSave}</Button>
        </Form>


        
        

      <Table striped bordered hover className="mt-4"> 
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>CreatedAt</th>
            <th>UpdatedAt</th>
            <th>Action</th>
          </tr>
        </thead>
        
        <tbody>
      
          {
              category.map((item,index)=>(
          <tr key={index}>
            <td>{item._id}</td>
            <td>{item.name}</td>
            <td>{item.createdAt}</td>
            <td>{item.updatedAt}</td>
            <td>
              <Button size="sm mr-2 mb-2 mt-2" variant="warning"
                onClick={
                  ()=>{
                   showText(item._id)
                  }
                }
              >Edit</Button>{" "}
              <Button size="sm " variant="danger" 
              onClick={()=>onDelete(item._id)} > Delete</Button>
            </td>
          </tr>
          ))}
      
        </tbody>
      </Table>
      </Container>
        
    )
}
