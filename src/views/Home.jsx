import React, { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllArticles } from '../redux/actions/articleAction'
import { fetchAllCategory} from '../redux/actions/categoryAction'
import { Button,Card ,Container,Row,Col} from "react-bootstrap";
import {NavLink, Redirect} from 'react-router-dom'
import { useHistory } from "react-router";

import { deleteArticle } from '../services/articles.services';
import { fetchPage } from '../services/pagination';



export default function Articles() {
    const dispatch = useDispatch()
    const history = useHistory()
    const {articles}  =useSelector(state=>state.articleReducer)
    const runCallback=(cb)=>{
       return cb();
    }
    useEffect(()=>{
        dispatch(fetchAllArticles())
    },[])

    const {category} = useSelector(state=>state.categoryReducer)
    useEffect(()=>{
        dispatch(fetchAllCategory())
    },[])
   
    const [articlePage,setArticlePage]=useState([])
    const[page,setPage]=useState(2)
    const[totalPge,setTotalPage]=useState(null)
    useEffect(()=>{
       fetchPage(page).then(response=>{
         setArticlePage(response.data)
         setTotalPage(response.total_page)
         console.log("TotalPage :",totalPge);
       })
    },[])

    
    const onDelete = (id)=>{
      deleteArticle(id).then((message)=>{
  
        let newArticles = articles.filter((article)=>article._id !== id)
        alert(message)
        articles = newArticles
      }).catch(e=>console.log(e))
    }

    let articleCard = articles.map((article) => (
        <Col md={3} key={article._id}>
          <Card className="my-2">
            <Card.Img
              variant="top"
              style={{ objectFit: "cover", height: "150px" }}
              src={article.image ? article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
            />
            <Card.Body>
              <Card.Title>{article.title}</Card.Title>
              <Card.Text className="text-line-3">
               {article.description}
              </Card.Text>
              <Button 
                size="sm" 
                variant="primary"
                onClick={()=>
                  history.push('/article/'+article._id)
                }
        
              >
                Read
              </Button>{" "}
              <Button 
                size="sm" 
                variant="warning"
                onClick={()=>{
                  history.push('/update/article/'+article._id)
                }}
                
              >
                Edit
              </Button>{" "}
              <Button size="sm" variant="danger" 
              onClick={()=>onDelete(article._id)}
              >
                Delete
              </Button>
            </Card.Body>
          </Card>
        </Col>
      ));
    
    return (
        
    <Container>
      <h4 className="my-4d">Category</h4>
      {
        category.map((item,idex)=>(
          <Button 
          size="sm" 
          variant="primary"
          className="mx-3"
        >
          {item.name}
        </Button>
        ))
      }
      <h5 className="text-center my-4"></h5>
      <Row>{articleCard}</Row>
      <Button className="my-5" style={{marginLeft:300}}>page</Button>
      {
        runCallback(()=>{
          const btn =[];
          for(var i=1; i<=totalPge;i++){
            btn.push(<NavLink to={`/articles?page=${i}&size=3`} className="mx-4"><button type="button" class="btn btn-outline-warning">{i}</button></NavLink>)
          }
          return btn;
        })
      }
    </Container>
        
    )
}
