
import React, { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllArticles } from '../redux/actions/articleAction'
import { Button,Card ,Container,Row,Col} from "react-bootstrap";
import { useHistory } from "react-router";
import {useLocation} from 'react-router-dom'
import { deleteArticle } from '../services/articles.services';
import { fetchPage } from '../services/pagination';
import queryString from 'query-string'

export default function ShowPage() {
    const location = useLocation()
    const page = queryString.parse(location.search)
    console.log("Page:",page);
    const dispatch = useDispatch()
    const {articles}  =useSelector(state=>state.articleReducer)
    useEffect(()=>{
        dispatch(fetchAllArticles())
    },[])
     
    const history = useHistory()
    const [articlePage,setArticlePage]=useState([])
    const[totalPge,setTotalPage]=useState(null)
    useEffect(()=>{
       fetchPage(page.page).then(response=>{
         setArticlePage(response.data)
         setTotalPage(response.total_page)
         console.log("TotalPage :",totalPge);
       })
    },[])


    const onDelete = (id)=>{
        deleteArticle(id).then((message)=>{
    
          let newArticles = articles.filter((article)=>article._id !== id)
          alert(message)
          articles = newArticles
        }).catch(e=>console.log(e))
      }
  

      
       let articleCardPage = articlePage.map(article=>
        <Col md={3} key={article._id}>
          
          <Card className="my-2">
          <Card.Img
            variant="top"
            style={{ objectFit: "cover", height: "150px" }}
            src={article.image ? article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}
          />
          <Card.Body>
            <Card.Title>{article.title}</Card.Title>
            <Card.Text className="text-line-3">
             {article.description}
            </Card.Text>
            <Button 
              size="sm" 
              variant="primary"
              onClick={()=>
                history.push('/article/'+article._id)
              }
      
            >
              Read
            </Button>{" "}
            <Button 
              size="sm" 
              variant="warning"
              onClick={()=>{
                history.push('/update/article/'+article._id)
              }}
              
            >
              Edit
            </Button>{" "}
            <Button size="sm" variant="danger" 
            onClick={()=>onDelete(article._id)}
            >
              Delete
            </Button>
          </Card.Body>
        </Card>
        </Col>
          )
      

    return (
        <div>
            
          <Container>
              <Row>
                  {articleCardPage } 
              </Row>
              <Button style={{width:200}} className="my-4" onClick={()=>history.goBack()}>Back To Home Page</Button>
          </Container>
      
        </div>
    )
}
