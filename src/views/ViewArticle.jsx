import React,{useState,useEffect} from 'react'
import { Col, Container, Row,Card,Form } from 'react-bootstrap'
import {useParams} from 'react-router'
import { fetchArticleById } from '../services/articles.services'
function ViewArticle() {

  const [article, setArticle] = useState()
  const {id} = useParams()
  console.log("Param ID",id);

  useEffect(() => {
    if(id){
      fetchArticleById(id).then(article=>{
        setArticle(article)
        console.log('Article',article);

      })
    }
  }, [])


  return (
    <Container>
        
        <Row className="my-5">
            {article ? <>
             <Col md={8}>
                  
                  <img className="w-100" src={article.image?article.image:"https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png"}/>
                  <h1 className="my-2">{article.title}</h1>
                  <p>{article.description}</p>
             </Col>
      

        </>:
        ''}
        <Col>
        <Form>
        <Form.Group controlId="description">
              <Form.Label><b>Comment</b></Form.Label>
              <Form.Control 
                as="textarea" 
                rows={4} 
                placeholder="comment here" 
               
              />
              
            </Form.Group>
        </Form>
        </Col>
        </Row>
    </Container>
  )
}

export default ViewArticle
